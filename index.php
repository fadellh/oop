<?php

require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");

echo $sheep->name .  " <br>" ; // "shaun"
echo $sheep->legs . " <br>"; // 2
echo $sheep->cold_blooded . " <br>"; // false
echo "<br>";
$sungokong = new Ape("kera sakti",2);
echo $sungokong->name ."<br>";
echo $sungokong->legs . " <br>";
$sungokong->yell() ;// "Auooo"
echo "<br>";
echo "<br>";
$kodok = new Frog("buduk",4);
echo $kodok->name . "<br>";
echo $kodok->legs. "<br>";
$kodok->jump() ; // "hop hop"
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>